<?php

namespace App\Entity;

use App\Repository\MovementRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MovementRepository::class)]
class Movement
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    private ?int $credit = null;

    #[ORM\Column(nullable: true)]
    private ?int $debit = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $comment = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCredit(): ?int
    {
        return $this->credit / 100;
    }

    public function setCredit(?int $credit): static
    {
        $this->credit = $credit * 100;

        return $this;
    }

    public function getDebit(): ?int
    {
        return $this->debit / 100;
    }

    public function setDebit(?int $debit): static
    {
        $this->debit = $debit * 100;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): static
    {
        $this->comment = $comment;

        return $this;
    }
}
